'use strict';

var myCarousel = $('.main-news__slider');
myCarousel.slick({
    slidesToShow: 8,
    slidesToScroll: 5,
    infinite: true,
    аccessibility: true,
    variableWidth: true,
    centerMode: true,
    speed: 300,
    dots: true,
    responsive: [
        {
            breakpoint: 1200,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
                infinite: true,
                centerMode: true,
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 2,
                centerMode: true,
            }
        },
        {
            breakpoint: 576,
            settings: {
                centerMode: true,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});

