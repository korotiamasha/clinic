$('.js-form-select').select2({
    minimumResultsForSearch: -1,
    placeholder: 'Из списка',
});

$(document).on('select2:open', 'select', function () {
    $('.select2-results').mCustomScrollbar({
        mouseWheel: true,
        theme: 'my-theme',
        advanced: {
            updateOnContentResize: true
        }
    });
});


$('.inputPhone').mask('+7 (999) 999  99  99', {placeholder: '+7 (___) ___  __  __ '});

$('.datepicker').datepicker({
    clearBtn: true,
    language: 'ru',
    minDate: new Date(),
    maxDate: '+1Y',
    startDate: new Date(),
    orientation: 'bottom left',
    todayHighlight: true,
    autoclose: true,
});

$('.timepicker').timepicker({
    minTime: '10',
    maxTime: '6:00pm',
    interval: 60,
});

$('.main-feedback__text .read-more').click(function () {
    if ($(this).parent().hasClass('show')) {
        $(this).parent().removeClass('show');
        $(this).html('Читать полностью');
    } else {
        $(this).parent().addClass('show');
        $(this).html('Скрыть');
    }
});

$('.header-min__search').click(function () {
    $('.header-min-form').addClass('show');
});

$('.input-group-append .btn-close').click(function () {
    $('.header-min-form').removeClass('show');
});


$('.header__main-link').mouseenter(function () {
    $('.main-menu').addClass('show');
}).mouseleave(function () {
    $('.main-menu').removeClass('show');
});


$('.contact-select').select2({
    minimumResultsForSearch: -1,
});

$('.select').select2({
    minimumResultsForSearch: -1,
});


$('.feed__more').click(function () {
    if ($(this).parent().hasClass('show')) {
        $(this).parent().removeClass('show');
        $(this).html('Читать полностью');
    } else {
        $(this).parent().addClass('show');
        $(this).html('Скрыть');
    }
});



$('.doctor-feedback__text .read-more').click(function () {
    if ($(this).parent().hasClass('show')) {
        $(this).parent().removeClass('show');
        $(this).html('Читать полностью');
    } else {
        $(this).parent().addClass('show');
        $(this).html('Скрыть');
    }
});

$('.price__filter-btn').click(function () {
    if ($(this).hasClass('active')) {
        $('.price__form').removeClass('show');
        $(this).removeClass('active');
    } else {
        $('.price__form').addClass('show');
        $(this).addClass('active');
    }
});


let mainBanner = $('.main-banner__slider');
mainBanner.slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: true,
    centerMode: false,
    speed: 300,
    dots: false,
});

var $mainstatus = $('.main-banner__slider-count');
var $slickBanner = $('.main-banner__slider');

$slickBanner.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
    var i = (currentSlide ? currentSlide : 0) + 1;

    $(this).parent().find('div.main-banner__slider-count').text(i + ' / ' + slick.slideCount);
});

let mainTrust = $('.main-trust__slider');
mainTrust.slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: true,
    centerMode: false,
    speed: 300,
    dots: false,
});

var $status = $('.main-trust__slider-count');
var $slickElement = $('.main-trust__slider');

$slickElement.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
    var i = (currentSlide ? currentSlide : 0) + 1;

    $(this).parent().find('div.main-trust__slider-count').text(i + ' / ' + slick.slideCount);
});


let slideNews = $('.main-news__slider');
slideNews.slick({
    slidesToShow: 8,
    slidesToScroll: 5,
    infinite: true,
    аccessibility: true,
    variableWidth: true,
    centerMode: true,
    speed: 300,
    dots: true,
    responsive: [
        {
            breakpoint: 1200,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
                infinite: true,
                centerMode: true,
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 2,
                centerMode: true,
            }
        },
        {
            breakpoint: 576,
            settings: {
                centerMode: true,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});

let sliderDoctor = $('.doctor-diplomas__slider');
sliderDoctor.slick({
    slidesToShow: 8,
    slidesToScroll: 5,
    infinite: true,
    аccessibility: true,
    variableWidth: true,
    centerMode: true,
    speed: 300,
    dots: true,
    responsive: [
        {
            breakpoint: 1200,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
                infinite: true,
                centerMode: true,
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 2,
                centerMode: true,
            }
        },
        {
            breakpoint: 576,
            settings: {
                centerMode: true,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});

let $statusDoctorD = $('.doctor-diplomas__slider-count');
let $slickDoctorD = $('.doctor-diplomas__slider');

$slickDoctorD.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
    let i = (currentSlide ? currentSlide : 0) + 1;

    $(this).parent().find('div.doctor-diplomas__slider-count').text(i + ' / ' + slick.slideCount);
});


let slideProgram = $('.main-program__slider');
slideProgram.slick({
    slidesToShow: 8,
    slidesToScroll: 5,
    infinite: true,
    аccessibility: true,
    variableWidth: true,
    centerMode: true,
    speed: 300,
    dots: true,
    responsive: [
        {
            breakpoint: 1200,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
                infinite: true,
                centerMode: true,
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 2,
                centerMode: true,
            }
        },
        {
            breakpoint: 576,
            settings: {
                centerMode: true,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});
var $statusProgram = $('.main-program__slider-count');
var $slickProgram = $('.main-program__slider');

$slickProgram.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
    var i = (currentSlide ? currentSlide : 0) + 1;

    $(this).parent().find('div.main-program__slider-count').text(i + ' / ' + slick.slideCount);
});


let slideDoctor = $('.main-doctor__slider');
slideDoctor.slick({
    slidesToShow: 8,
    slidesToScroll: 5,
    infinite: true,
    аccessibility: true,
    variableWidth: true,
    centerMode: true,
    speed: 300,
    dots: true,
    responsive: [
        {
            breakpoint: 1200,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
                infinite: true,
                centerMode: true,
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 2,
                centerMode: true,
            }
        },
        {
            breakpoint: 576,
            settings: {
                centerMode: true,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});

var $statusDoctor = $('.main-doctor__slider-count');
var $slickDoctor = $('.main-doctor__slider');

$slickDoctor.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
    var i = (currentSlide ? currentSlide : 0) + 1;

    $(this).parent().find('div.main-doctor__slider-count').text(i + ' / ' + slick.slideCount);
});

let slideFeedback = $('.main-feedback__slider');
slideFeedback.slick({
    slidesToShow: 8,
    slidesToScroll: 5,
    infinite: true,
    аccessibility: true,
    variableWidth: true,
    centerMode: true,
    speed: 300,
    dots: true,
    responsive: [
        {
            breakpoint: 1200,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
                infinite: true,
                centerMode: true,
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 2,
                centerMode: true,
            }
        },
        {
            breakpoint: 576,
            settings: {
                centerMode: true,
                variableWidth: true,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});





var $statusFeedback = $('.main-feedback__slider-count');
var $slickFeedback = $('.main-feedback__slider');

$slickFeedback.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
    var i = (currentSlide ? currentSlide : 0) + 1;

    $(this).parent().find('div.main-feedback__slider-count').text(i + ' / ' + slick.slideCount);
});


let $statusNews = $('.main-news__slider-count');
let $slickNews = $('#main-news__slider');

$slickNews.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
    let i = (currentSlide ? currentSlide : 0) + 1;

    $(this).parent().find('div.main-news__slider-count').text(i + ' / ' + slick.slideCount);
});


let $statusStocks = $('.main-stocks__slider-count');
let $slickStocks = $('#main-stocks__slider');

$slickStocks.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
    let i = (currentSlide ? currentSlide : 0) + 1;

    $(this).parent().find('div.main-stocks__slider-count').text(i + ' / ' + slick.slideCount);
});

let sliderMainNews = $('#main-news__slider');
sliderMainNews.slick({
    slidesToShow: 8,
    slidesToScroll: 5,
    infinite: true,
    аccessibility: true,
    variableWidth: true,
    centerMode: true,
    speed: 300,
    dots: true,
    responsive: [
        {
            breakpoint: 1200,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
                infinite: true,
                centerMode: true,
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 2,
                centerMode: true,
            }
        },
        {
            breakpoint: 576,
            settings: {
                centerMode: true,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});



let sliderMainStocks = $('#main-stocks__slider');
sliderMainStocks.slick({
    slidesToShow: 8,
    slidesToScroll: 5,
    infinite: true,
    аccessibility: true,
    variableWidth: true,
    centerMode: true,
    speed: 300,
    dots: true,
    responsive: [
        {
            breakpoint: 1200,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
                infinite: true,
                centerMode: true,
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 2,
                centerMode: true,
            }
        },
        {
            breakpoint: 576,
            settings: {
                centerMode: true,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});





$('.faq__header').click(function () {
    if ($(this).closest('div.faq__item').hasClass('active')) {
        $(this).closest('div.faq__item').removeClass('active');
    } else {
        $(this).closest('div.faq__item').addClass('active');
    }
});


$('.sidebar-list__more').click(function (e) {
    e.preventDefault();
    let listItem = $(this).parent();
    if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        listItem.removeClass('active');
        listItem.next().removeClass('active');
    } else {
        $(this).addClass('active');
        listItem.addClass('active');
        listItem.next().addClass('active');
    }
});

