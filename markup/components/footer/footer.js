$('.footer-min-btn').click(function () {
    if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        $(this).parent().find('.footer__list').removeClass('show');
    } else {
        $(this).addClass('active');
        $(this).parent().find('.footer__list').addClass('show');
    }
});
