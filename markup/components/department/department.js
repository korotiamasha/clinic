var $status = $('.price-block-min  .price-block__slider-count');
var $slickElement = $('.price-block-min .price-block__slider');

$slickElement.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
    var i = (currentSlide ? currentSlide : 0) + 1;

    $(this).parent().find('div.price-block__slider-count').text(i + ' / ' + slick.slideCount);
});

let priceSlider = $('.price-block__slider');
priceSlider.slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: true,
    infinite: true,
    speed: 200,
});

var $statusDep = $('.department__menu-slider-wrap');
var $slickDep = $('.department__menu-slider-count');

$slickDep.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
    var i = (currentSlide ? currentSlide : 0) + 1;

    $(this).parent().find('div.department__menu-slider-count').text(i + ' / ' + slick.slideCount);
});

let depSlider = $('.department__menu-slider-wrap');
depSlider.slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: true,
    infinite: true,
    speed: 200,
});
